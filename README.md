# Playwright JS

Getting started

## Steps
- `npm init -y`  to init the javascript project
- `npm i -D playwright` to install the playwright dependency
- fist example using the next file `first.js`
- to run the first test execute the next coommand `node first.js`
## Author ✒️

* **Henry Andrés Correa Correa** - [Linkedin](https://www.linkedin.com/in/henryandrescorrea/) -  [h.andresc1127@gmai.com](mailto:h.andresc1127@gmai.com) \