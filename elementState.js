const { chromium, firefox } = require('playwright');

(async() => {
    const browser = await chromium.launch({headless:false});
    const page = await browser.newPage();
    await page.goto('https://demoqa.com/automation-practice-form');

    const firtName = await page.$('#firstName');
    const sportCheck = await page.$('#hobbies-checkbox-1');
    const submit = await page.$('#submit');

    console.log(await firtName.isDisabled());
    console.log(await firtName.isEnabled());
    console.log(await firtName.isEditable());
    console.log(await sportCheck.isChecked());
    console.log(await sportCheck.isVisible());
    console.log(await submit.isHidden());
    console.log(await submit.isVisible());


    browser.close();
})();